using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Entities
{
    public class Venda
    {
        public int Id { get; set; }
        public string Vendedor { get; set; }
        public DateTime DataVenda { get; set; }
        public EnumStatus StatusVenda { get; set; }
    }
}