using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Entities;
using Microsoft.EntityFrameworkCore;

namespace tech_test_payment_api.Context
{
    public class GerenciadorDeVendasContext : DbContext
    {
        public GerenciadorDeVendasContext(DbContextOptions<GerenciadorDeVendasContext> options) : base(options)
        {

        }

        DbSet<Venda> Vendas { get; set; }
        DbSet<Vendedor> Vendedores { get; set; }
    }
}